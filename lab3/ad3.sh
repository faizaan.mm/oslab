file=$1
counter=0
 
eout="evenfile.txt"
oout="oddfile.txt"
 
if [ $# -eq 0 ] 
then
 echo "$(basename $0) file"
 exit 1 
fi
 
if [ ! -f $file ]
then
 echo "$file not a file"
 exit 2
fi
 
while read line
do
 isEvenNo=$(($counter % 2))
 
 if [ "$isEvenNo" == 0 ] 
 then
 
 echo $line >> $eout
 else
 
 echo $line >> $oout 
 fi
 
 (( counter ++ ))
done < $file
echo "Even file - $eout"
echo "Odd file - $oout"
