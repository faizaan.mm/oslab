#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include<string.h>
#include <sys/wait.h>
int main(){

    int status;
    int n1,n2,n,i,j;
    int pid2;
    int return_code;
    printf("Enter the number of words \n");
    scanf("%i", &n);
    char argv[20][20];
    printf("Enter words: ");
    for(i=0;i<n;i++)
    {
        scanf("%s", argv[i]);
    }
     n1=fork();
     n2=fork();

     if(n1==0 && n2==0)
         {
         wait(&status);
             char temp[50];
             printf("Bubble sort: \n");
             for(i=0;i<n;i++){
                 for(j=0;j<n-1;j++){
                     if(strcmp(argv[j],argv[j+1])>0){
                         strcpy(temp,argv[j]);
                         strcpy(argv[j],argv[j+1]);
                         strcpy(argv[j+1],temp);
                     }
                 }
             }
             for(i=0;i<n;i++)
                 printf("%s\n",argv[i]);
         }

     if(n1==0 && n2>0){
         wait(&status);
         printf("\nSelection sort:\n");
         int i, j, min_idx;
         char minStr[n];
         for (i = 0; i < n-1; i++)
         {

             int min_idx = i;
             strcpy(minStr, argv[i]);
             for (j = i+1; j < n; j++)
             {
                 if (strcmp(minStr, argv[j]) > 0)
                 {
                     
                     strcpy(minStr, argv[j]);
                     min_idx = j;
                 }
             }

             if (min_idx != i)
             {
                 char temp[50];
                 strcpy(temp, argv[i]); 
                 strcpy(argv[i], argv[min_idx]);
                 strcpy(argv[min_idx], temp);
             }
         }
          for(i=0;i<n;i++)
     printf("%s\n",argv[i]);
     
     }
    if(n1!=0 && n2!=0)
         {
             wait(&status);
             printf(" \nParent process terminated");
         }
    
}
