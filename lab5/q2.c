 #include<stdio.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<string.h>
#include<unistd.h>

int main(int argc, char *argv[]){
	char arr[50][50],t[20];
	pid_t pid = fork();
	int i,j,status;
	for(i = 1; i<argc; i++){
		strcpy(arr[i-1], argv[i]);
	}
	if(pid == 0){
		for(i = 1; i<=argc; i++){
			for(j = 1; j<=argc -i; j++){
				if(strcmp(arr[j-1],arr[j]) > 0){
					strcpy(t,arr[j-1]);
					strcpy(arr[j-1],arr[j]);
					strcpy(arr[j],t);
					}
				}
			}
		printf("\nSorted list: ");
		for(i = 1; i<argc; i++){
			printf("%s\n",arr[i]);
		}
	}
	else if(pid > 0){
		wait(NULL);
		printf("\nUnsorted list: ");
		for(i = 1; i<argc; i++){
			printf("%s\n",argv[i]);
		}	
	}
	return 0;
}
