#include<stdio.h>
#include<sys/types.h>
#include<unistd.h>
int main()
{
	int status;
	pid_t pid;
	pid=fork();
	if(pid>1)
	{
	wait(&status);
	printf("Parent \n Pid");
	printf("%ld",(long)getpid());
	printf("\n PPid");
	printf("%ld",(long)getppid());
	printf("\n");
	}
	if(pid==0)
	{
	printf("Child \n Pid");
	printf("%ld",(long)getpid());
	printf("\n PPid");
        printf("%ld",(long)getppid());
	printf("\n");
	}
	return 0;
	}

