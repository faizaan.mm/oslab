#include<stdlib.h>
#include<stdio.h>
int main()
{
	int np,nh,i,j,max,flag=0;
	int *h;
	int *p;
	int *alp;
	int *alh;
	int *free;
	printf("Enter the number of holes \n");
	scanf("%d",&nh);
	printf("Enter the number of processes \n");
	scanf("%d",&np);
	p = (int *)malloc(sizeof(int)*np);
	alp = (int *)malloc(sizeof(int)*np);
	h = (int *)malloc(sizeof(int)*nh);
	alh = (int *)malloc(sizeof(int)*nh);
	free = (int *)malloc(sizeof(int)*nh);
	printf("enter the size of each hole \n");
	for(i=0;i<nh;i++)
	{
		int x;
		scanf("%d",&x);
		h[i]=x;
		alh[i]=-1;
		free[i]=h[i];

	}
	printf("enter the size of each process \n");
	for(i=0;i<np;i++)
	{
		int x;
		scanf("%d",&x);
		p[i]=x;
		alp[i]=-1;
	}
	
	for(i=0;i<np;i++)
	{	flag=0;
		for(j=0;j<nh;j++)
		{
			if(p[i]<free[j])
			{
				if(flag==0)
					{
						max=j;
						flag=1;
					}
				else
				{
					if(free[j]>free[max])
					{
						max=j;
					}
				}			
			}
		}
		free[max]-=p[i];
		if(free[max]<0)
		{
			free[max]+=p[i];
			continue;
		}
		alp[i]=max;
		alh[max]=i;
	}
	printf("\n hole details \n");
	for(i=0;i<nh;i++)
	{
		//if(alh[i]!=-1)
		{
			printf(" \n%d\n ",i);
			printf(" Size=%d ",h[i]);
			printf(" free=%d ",free[i]);
			printf(" process=%d ",alh[i]);
		}	
	}
	printf("\nprocess details\n");
	for(i=0;i<np;i++)
	{
		if(alp[i]!=-1)
		{
			printf("\n%d\n",i);
			printf(" Size=%d ",p[i]);
			printf(" hole=%d",alp[i]);
		}
		else
		{
			printf("\n%d\n",i);
			printf(" Size=%d ",p[i]);
			printf(" unallocated ");
		}
	}
	return 0;
}