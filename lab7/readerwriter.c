#include<stdio.h>
#include<semaphore.h>
#include<pthread.h>
#include<stdlib.h>

sem_t mutex,xwrite;
int readcount;
void *reader(void *);
void *writer(void *);
FILE *fptr;
int numw=0;
void *reader(void *arg)
{

    int temp = (int)arg;
    int num;
    printf("\nReader %d trying to read", temp);
    sem_wait(&mutex);
    readcount++;
    if(readcount==1)
    {
        sem_wait(&xwrite);
        fptr=fopen("./test.txt","r");
    }

    sem_post(&mutex);
    printf("\nReader %d active", temp);
    fscanf(fptr,"%d",&num);
    printf("\nReader Value %d", num);
    sem_wait(&mutex);
    readcount--;
    printf("\nReader %d exiting", temp);

    if(readcount==0)
    {
       fclose(fptr);
       sem_post(&xwrite);
    }
    sem_post(&mutex);
}

void *writer(void *arg)
{
    numw++;
    int temp = (int)arg;
    printf("\nWriter %d trying write", temp);
    sem_wait(&xwrite);
    fptr=fopen("./test.txt","w");
    printf("\nWriter %d active", temp);
    fprintf(fptr,"%d",numw);
    printf("\nWriter %d exiting", temp);
    fclose(fptr);
    sem_post(&xwrite);
}

int main()
{
    int r=0,w=0,i=0;
    sem_init(&xwrite,0,1);
    sem_init(&mutex,0,1);
    readcount = 0;
    pthread_t *readers, *writers;
    printf("enter number of readers\n");
    scanf("%d",&r);
    printf("enter number of writers\n");
    scanf("%d",&w);
    readers = (pthread_t *)malloc(sizeof(pthread_t) *r);
    writers = (pthread_t *)malloc(sizeof(pthread_t) *w);
    for(i=0;i<r;i++)
    {
     pthread_create(&readers[i], NULL, &reader, (void *)i);
    }
    for(i=0;i<w;i++)
    {
     pthread_create(&writers[i], NULL, &writer, (void *)i);
    }
    for(i=0;i<r;i++)
    {
     pthread_join(readers[i], NULL);
    }
    for(i=0;i<w;i++)
    {
     pthread_join(writers[i], NULL);
    }
    printf("\n");
    return 0;
}
